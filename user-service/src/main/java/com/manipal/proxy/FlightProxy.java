package com.manipal.proxy;

import java.util.List;

import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.manipal.model.Booking;




//@FeignClient(name="flight-search-booking-service")
//@RibbonClient(name="flight-search-booking-service")
@FeignClient(name="zuul-api-gateway")
@RibbonClient(name="flight-search-booking-service")
public interface FlightProxy {
	
	@PostMapping("flight-search-booking-service/booking/addBooking")
	public Booking addBookingDetails(@RequestBody Booking booking);
	
	@PutMapping("flight-search-booking-service/booking/cancelbooking/{bookingId}")
	public boolean deleteBooking(@PathVariable int bookingId);
	
	@GetMapping("flight-search-booking-service/booking/getdetailsbyid/{personId}")
	public List<Booking> getBookingDetails(@PathVariable int personId);
}
