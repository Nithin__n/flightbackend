package com.manipal.model;

import java.util.List;

public class Dashboard {
	User user;
	List<Booking> booking;
	public Dashboard() {
		super();
	}
	public Dashboard(User user, List<Booking> booking) {
		super();
		this.user = user;
		this.booking = booking;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public List<Booking> getBooking() {
		return booking;
	}
	public void setBooking(List<Booking> booking) {
		this.booking = booking;
	}
	
}
