package com.manipal.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
@Embeddable
public class Contact {
	private Address address;
	private String email;
	@Column(name="mobile_no")
	private String mobileNo;
	public Contact() {
		super();
	}
	

	public Contact(Address address, String email, String mobileNo) {
		super();
		this.address = address;
		this.email = email;
		this.mobileNo = mobileNo;
	}


	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}


	public String getMobileNo() {
		return mobileNo;
	}


	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	
	
}
