package com.manipal.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.manipal.model.Booking;
import com.manipal.model.Dashboard;
import com.manipal.model.User;
import com.manipal.service.IUserService;

@RestController
@CrossOrigin
@RequestMapping("/user")
public class UserController {
	
	@Autowired
	IUserService userService;
	
	@PostMapping("registration")
	public User registerUser(@RequestBody User user)
	{
		return userService.registerUser(user);
	}
	
	@GetMapping("login/{userName}/{password}")
	public User userLogin(@PathVariable String userName , @PathVariable String password)
	{
		return userService.loginUser(new User(userName,password));
	}
	
	@PutMapping("password")
	public User resetPassword(@RequestBody User user)
	{
		return userService.resetPassword(user);
	}
	
	@PutMapping
	public User updateUser(@RequestBody User user)
	{
		return userService.updateUser(user);
	}
	
	@PostMapping("booking")
	public Booking bookTickets(@RequestBody Booking booking)
	{
		return userService.bookTickets(booking);
	}
	
	@DeleteMapping("{userId}/booking/{bookingId}")
	public boolean cancelTickets(@PathVariable int userId,@PathVariable int bookingId)
	{
		return userService.cancelTickets(userId, bookingId);
	}
	
	@GetMapping("{userId}/dashboard")
	public Dashboard getDashboard(@PathVariable int userId)
	{
		return userService.getDashboard(userId);
	}
	
	@GetMapping("{userId}")
	public User getUser(@PathVariable int userId)
	{
		return userService.getUser(userId);
		
	}
}
