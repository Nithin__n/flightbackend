package com.manipal.service;

import com.manipal.model.Booking;
import com.manipal.model.Dashboard;
import com.manipal.model.User;

public interface IUserService {

	User registerUser(User user);

	User loginUser(User user);

	User resetPassword(User user);

	User updateUser(User user);

	Booking bookTickets(Booking booking);

	boolean cancelTickets(int userId, int bookingId);

	Dashboard getDashboard(int userId);

	User getUser(int userId);

}
