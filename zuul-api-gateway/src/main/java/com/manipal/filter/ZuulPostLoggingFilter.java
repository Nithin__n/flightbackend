package com.manipal.filter;


import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;

@Component
public class ZuulPostLoggingFilter extends ZuulFilter{
	
	private Logger logger = LoggerFactory.getLogger(ZuulPostLoggingFilter.class);
	
	@Override
	public boolean shouldFilter() {
		return true;
	}

	@Override
	public Object run() throws ZuulException {
		HttpServletRequest request = RequestContext.getCurrentContext().getRequest();
		logger.info("============================Microservices post log filter starts here============================");
		logger.info("request ==> {}",request);
		logger.info("request character encoding ==> {}",request.getCharacterEncoding());
		logger.info("server name ==> {}",request.getServerName());
		logger.info("============================Microservices post log filter ends here============================");
		return null;
	}

	@Override
	public String filterType() {
		return "post";
	}

	@Override
	public int filterOrder() {
		return 1;
	}

}
